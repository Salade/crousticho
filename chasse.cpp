#include "chasse.hpp"
#include "hud.hpp"

std::vector<std::pair<int,int>> Chasse::_dimension;

Chasse::Chasse()
:Batiment("Chasse", 60, 5, 2, 2 * buffFood)
{
	_ressource = {{0,30,15,2,0,0}};
}

void Chasse::construction(std::array<int, 6>& stock)
{
	int i;

	for (i=0;i<6;i++){
		stock[i] -= _ressource[i];
	}
}

void Chasse::upgrade(std::array<int, 6>& stock)
{
	if (_niveau < _nivMax){

		switch (_niveau)
		{
			case 1:
				_ressource = {{15, 50, 35, 0, 0, 0}};
				_cptFrameMax = 58;
				break;
			case 2:
				_ressource = {{40, 130, 120, 1, 0, 0}};
				_cptFrameMax = 55;
				break;
			case 3:
				_ressource = {{80, 240, 200, 1, 0, 0}};
				_cptFrameMax = 50;
				break;
			case 4:
				_ressource = {{140, 170, 155, 1, 0, 0}};
				_cptFrameMax = 45;
				break;
			default:
				break;
		}

		if(Hud::getHud()->enoughRessources(Hud::getHud()->getRessourceStockees(), _ressource))
		{
			construction(stock);
			_niveau++;
		}
	}
}

void Chasse::majRessource(std::array<int, 6>& stock)
{

	if (_cptFrame == _cptFrameMax){
		if ((stock[BOIS] >= 2) && (stock[PIERRE] >= 1)){
			stock[BOIS] -= 2;
			stock[PIERRE] -= 1;
			stock[BOUFFE] += 2 * buffFood;
			_cptFrame = 0;
		}
	}else{
		_cptFrame++;
	}
}

const std::vector<std::pair<int,int>>& Chasse::getDimension()
{
	return _dimension;
}

void Chasse::setDimension(const std::vector<std::pair<int,int>>& dimension)
{
	_dimension = dimension;
}

void Chasse::init(){
	std::vector<std::pair<int, int>> temp;

	int i,j;

	for (i=0;i<2;i++){
		for (j=0;j<2;j++){
			temp.push_back(std::make_pair(i,j));
		}
	}

	Chasse::setDimension(temp);
}

batiments Chasse::type()
{
	return CHASSE;
}
