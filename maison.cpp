#include "maison.hpp"
#include "hud.hpp"
#include <time.h>

std::vector<std::pair<int,int>> Maison::_dimension;

Maison::Maison()
: Batiment("Maison", 180, 5, 0, 0), _capaPopulation(5), _habitants(1), _famine(1.0)
{
  //30 GLACE
  _ressource[0] = 30;
  //10 BOIS
  _ressource[1] = 10;
  //0 PIERRE
  _ressource[2] = 0;
  //0 PERSONNE
  _ressource[3] = 0;
  //CHARBON
  _ressource[4] = 0;
  //BOUFFE
  _ressource[5] = 0;
}

void Maison::construction(std::array<int, 6>& stock)
{
  for(int i = 0; i < 6; ++i)
  {
    stock[i] -= _ressource[i];
  }
}

void Maison::upgrade(std::array<int, 6>& stock)
{
	if (_niveau < _nivMax){

		switch (_niveau)
		{
			case 1:
				_ressource = {{40, 30, 20, 0, 0, 0}};
				break;
			case 2:
				_ressource = {{50, 50, 40, 0, 0, 0}};
				break;
			case 3:
				_ressource = {{80, 100, 90, 0, 0, 0}};
				break;
			case 4:
				_ressource = {{140, 170, 155, 0, 0, 0}};
				break;
			default:
				break;
		}
    if(Hud::getHud()->enoughRessources(Hud::getHud()->getRessourceStockees(), _ressource))
    {
      construction(stock);
      _niveau++;
    }
	}
}

void Maison::majRessource(std::array<int, 6>& stock)
{
	float gonnaDie;

  //std::cout << _cptFrame << std::endl;

	if(_cptFrame >= _cptFrameMax)
  {
		if (_habitants < _capaPopulation)
    {
      //BUG TROUVE !
      //stock[PERSONNE] = ++_habitants;

      ++_habitants;
			++stock[PERSONNE];
      ++_conso;
		}
		_cptFrame = 0;
	}
	else
	{
		++_cptFrame;
	}

	if(_habitants > 0)
  {
		if(_cptFrame % 90 == 0)
    { // Vérif toutes les mn
			if ((stock[BOUFFE] >= _habitants) && (stock[CHARBON] >= 1))
      {
        Hud::getHud()->setHabitantMort(false);
				stock[BOUFFE] -= _habitants;
				stock[CHARBON] -= 1;
				_famine = 0;
			}
      else
      {
				gonnaDie = (float)rand()/RAND_MAX;
				if (gonnaDie < (1-1.0/_famine))
        {
          Hud::getHud()->setHabitantMort(true);
          //std::cout << "mort d'un habitant" << std::endl;
					--stock[PERSONNE];
					--_habitants;
          --_conso;
				}

        ++_famine;

			}
		}
	}
}

const std::vector<std::pair<int,int>>& Maison::getDimension()
{
	return _dimension;
}

void Maison::setDimension(const std::vector<std::pair<int,int>>& dimension)
{
	_dimension = dimension;
}

void Maison::init()
{
  std::vector<std::pair<int, int>> temp;

  temp.push_back(std::make_pair(0, 0));

  Maison::setDimension(temp);
}

batiments Maison::type()
{
  return MAISON;
}
