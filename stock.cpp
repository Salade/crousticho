#include "stock.hpp"

std::vector<std::pair<int,int>> Stock::_dimension;

int Stock::_nbStocks = 0;

Stock::Stock()
: Batiment("Stock", 0, 1, 0, 0), _capaStockage(6000)
{
  _ressource[GLACE] = 5;
  _ressource[BOIS] = 15;
  _ressource[PIERRE] = 25;
  _ressource[PERSONNE] = 1;
  _ressource[CHARBON] = 0;
  _ressource[BOUFFE] = 0;

  _nbStocks++;
}

void Stock::construction(std::array<int, 6>& stock){
	  for(int i = 0; i < 6; ++i)
  {
    stock[i] -= _ressource[i];
  }
}

void Stock::upgrade(std::array<int, 6>&)
{

}

void Stock::majRessource(std::array<int, 6>&)
{

}


const std::vector<std::pair<int,int>>& Stock::getDimension()
{
	return _dimension;
}

void Stock::setDimension(const std::vector<std::pair<int,int>>& dimension)
{
	_dimension = dimension;
}

void Stock::init()
{
  std::vector<std::pair<int, int>> temp;

  temp.push_back(std::make_pair(0, 0));
  temp.push_back(std::make_pair(1, 0));
  temp.push_back(std::make_pair(2, 0));
  temp.push_back(std::make_pair(0, 1));
  temp.push_back(std::make_pair(1, 1));
  temp.push_back(std::make_pair(2, 1));
  temp.push_back(std::make_pair(0, 2));
  temp.push_back(std::make_pair(1, 2));
  temp.push_back(std::make_pair(2, 2));

  setDimension(temp);
}

batiments Stock::type()
{
  return STOCK;
}

int Stock::getNbStocks()
{
	return _nbStocks;
}
