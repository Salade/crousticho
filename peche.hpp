#ifndef PECHE_CPP
#define PECHE_CPP

#include "data.hpp"
#include "batiment.hpp"

class Peche : public Batiment
{
  private:

    static std::vector<std::pair<int,int>> _dimension;

  public:

    Peche();

    static const std::vector<std::pair<int,int>>& getDimension();

    static void setDimension(const std::vector<std::pair<int,int>>&);

    void construction(std::array<int, 6>&);

    void upgrade(std::array<int, 6>&);

    void majRessource(std::array<int, 6>&);

    static void init();

    batiments type();
};
#endif
