#ifndef HUD_HPP
#define HUD_HPP

#include "Map.h"


class Hud
{
  private:

    sf::RenderWindow* _window;

    sf::Texture _texture;

    sf::Sprite _sprite;

    sf::Texture _textureSelection;

    sf::Sprite _spriteSelection;

    static Hud * _hud;

    std::vector<Batiment *> _batiments;

    sf::View _camera;

    std::array<int, 6> _ressourceStockees;
    std::array<int, 6> _changements;

    int _stockMax;

    int _xCamera, _yCamera;

    int _up, _down, _left, _right;

    int _choix;

    Map * _map;

    sf::Font _font;

    int _frame, _frameMax;

    bool _stockPlein, _habitantMort;

    sf::Sprite _spriteVisual;

  public:

    Hud(sf::RenderWindow*, Map*);
    ~Hud();

    void init(sf::RenderWindow*);

    void start();

    static Hud * getHud();

    void tick();

    void keyPressed(const sf::Event&);
    void keyReleased(const sf::Event&);
    void clickReleased(const sf::Event&);
    void mouseMoved(const sf::Event&);

    void moveCamera();
    void drawButton();

    void addBatiment(Batiment * bat);

    void getEvents();

    void majStock();

    bool enoughRessources(const std::array<int, 6>&, const std::array<int, 6>&);

    void writeRessources() const;

    const std::array<int, 6>& getRessourceStockees() const;

    void setHabitantMort(const bool& b);
};

#endif
