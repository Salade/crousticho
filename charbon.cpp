#include "charbon.hpp"
#include "hud.hpp"

std::vector<std::pair<int,int>> Charbon::_dimension;

Charbon::Charbon()
: Batiment("Charbon", 20, 5, 0, 1 * buffFood)
{//salut //ça va ? // non.... // c'est parce que le web c'est de la merde // pire // le split ? // le web c pire
  _ressource[GLACE] = 0;
  _ressource[BOIS] = 20;
  _ressource[PIERRE] = 30;
  _ressource[PERSONNE] = 3;
  _ressource[CHARBON] = 0;
  _ressource[BOUFFE] = 0;
}

void Charbon::construction(std::array<int, 6>& stock)
{
  for(int i = 0; i < 6; ++i)
  {
    stock[i] -= _ressource[i];
  }
}

void Charbon::upgrade(std::array<int, 6>& stock)
{
	if (_niveau < _nivMax){

		switch (_niveau)
		{
			case 1:
				_ressource = {{40, 65, 80, 0, 0, 0}};
				_cptFrameMax = 19;
				break;
			case 2:
				_ressource = {{90, 145, 170, 0, 0, 0}};
				_cptFrameMax = 18;
				break;
			case 3:
				_ressource = {{135, 225, 290, 1, 0, 0}};
				_cptFrameMax = 17;
				break;
			case 4:
				_ressource = {{190, 310, 395, 1, 0, 0}};
				_cptFrameMax = 15;
				break;
			default:
				break;
		}
    if(Hud::getHud()->enoughRessources(Hud::getHud()->getRessourceStockees(), _ressource))
    {
      construction(stock);
      _niveau++;
    }
	}
}

void Charbon::majRessource(std::array<int, 6>& stock)
{
  if(_cptFrame == _cptFrameMax)
  {
    stock[CHARBON] += 1 * buffFood;

    _cptFrame = 0;
  }
  else
  {
    ++_cptFrame;
  }
}


const std::vector<std::pair<int,int>>& Charbon::getDimension()
{
	return _dimension;
}

void Charbon::setDimension(const std::vector<std::pair<int,int>>& dimension)
{
	_dimension = dimension;
}

void Charbon::init()
{
  std::vector<std::pair<int, int>> temp;

  temp.push_back(std::make_pair(0, 0));
  temp.push_back(std::make_pair(1, 0));
  temp.push_back(std::make_pair(0, 1));
  temp.push_back(std::make_pair(0, 2));
  temp.push_back(std::make_pair(1, 2));

  setDimension(temp);
}

batiments Charbon::type()
{
  return MCHARBON;
}
