#ifndef __BATIMENT__
#define __BATIMENT__

#include "data.hpp"

enum batiments {MAISON, CHASSE, PECHE, BOISSON, SERRE, MPIERRE, MGLACE, MCHARBON, STOCK, LOISIR, EDUCATION};

class Batiment
{

	// ATTRIBUTS //

	protected :
		std::string _nom;
		std::array<int, 6> _ressource; //{glace,bois,pierre,personne,charbon,bouffe}
		int _cptFrame = 0, _cptFrameMax;
		int _niveau, _nivMax;
		int _conso, _prod;


	public :
		int _croustichO;


	// METHODES //

	//Methode temps

	public :

		Batiment(const std::string nom, const int& cptFrameMax, const int& nivMax = 1, const int& conso = 0 ,const int& prod = 0);
		virtual ~Batiment(){};

		std::string getNom() const;
		int getNiveau() const;
		const std::array<int, 6>& getRessource();

		const int& getConso() const;
		const int& getProd() const;

		void draw() const;

		virtual void construction(std::array<int, 6> &) = 0;
		virtual void upgrade(std::array<int, 6> &) = 0;
		virtual void majRessource(std::array<int, 6> &) = 0;
		virtual batiments type() = 0;

};


#endif
