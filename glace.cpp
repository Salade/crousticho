#include "glace.hpp"
#include "hud.hpp"

std::vector<std::pair<int,int>> Glace::_dimension;

Glace::Glace()
: Batiment("Glace", 20, 5, 0, 1 * buffFood)
{
  _ressource[GLACE] = 0;
  _ressource[BOIS] = 15;
  _ressource[PIERRE] = 20;
  _ressource[PERSONNE] = 3;
  _ressource[CHARBON] = 0;
  _ressource[BOUFFE] = 0;
}

void Glace::construction(std::array<int, 6>& stock)
{
  for(int i = 0; i < 6; ++i)
  {
    stock[i] -= _ressource[i];
  }
}

void Glace::upgrade(std::array<int, 6>& stock)
{
	if (_niveau < _nivMax){

		switch (_niveau)
		{
			case 1:
				_ressource = {{20, 45, 55, 0, 0, 0}};
				_cptFrameMax = 19;
				break;
			case 2:
				_ressource = {{40, 100, 130, 0, 0, 0}};
				_cptFrameMax = 18;
				break;
			case 3:
				_ressource = {{65, 170, 195, 1, 0, 0}};
				_cptFrameMax = 17;
				break;
			case 4:
				_ressource = {{80, 255, 300, 1, 0, 0}};
				_cptFrameMax = 15;
				break;
			default:
				break;
		}
    if(Hud::getHud()->enoughRessources(Hud::getHud()->getRessourceStockees(), _ressource))
    {
      construction(stock);
      _niveau++;
    }
	}
}

void Glace::majRessource(std::array<int, 6>& stock)
{
  if(_cptFrame == _cptFrameMax)
  {
    stock[GLACE] += 1 * buffFood;

    _cptFrame = 0;

  }
  else
  {
    ++_cptFrame;
  }
}


const std::vector<std::pair<int,int>>& Glace::getDimension()
{
	return _dimension;
}

void Glace::setDimension(const std::vector<std::pair<int,int>>& dimension)
{
	_dimension = dimension;
}

void Glace::init()
{
  std::vector<std::pair<int, int>> temp;

  temp.push_back(std::make_pair(0, 0));
  temp.push_back(std::make_pair(0, 1));
  temp.push_back(std::make_pair(1, 0));
  temp.push_back(std::make_pair(0, 2));
  temp.push_back(std::make_pair(1, 1));
  temp.push_back(std::make_pair(1, 2));

  setDimension(temp);
}

batiments Glace::type()
{
  return MGLACE;
}
