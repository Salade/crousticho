#ifndef STOCK_CPP
#define STOCK_CPP

#include "data.hpp"
#include "batiment.hpp"

class Stock : public Batiment
{
  private:

    static std::vector<std::pair<int,int>> _dimension;

    int _capaStockage;

    static int _nbStocks;

  public:

    Stock();

    static const std::vector<std::pair<int,int>>& getDimension();

    static void setDimension(const std::vector<std::pair<int,int>>&);

    void construction(std::array<int, 6>&);

    void upgrade(std::array<int, 6>&);

    void majRessource(std::array<int, 6>&);

    static void init();

    batiments type();

    static int getNbStocks();
};
#endif
