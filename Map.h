#ifndef MAP_H
#define MAP_H

#include "data.hpp"
#include "batiment.hpp"

class Map
{
    public:
        Map(sf::RenderWindow* window);
        virtual ~Map();
        void draw();
        void addBatiment(Batiment * batiment, int x, int y);
        bool isEmpty(const std::vector<std::pair<int,int> > &dim, int x, int y);
        void addBatimentMap(Batiment * b,const std::vector<std::pair<int,int>> & dim, int x, int y);
        const sf::Texture& getTextureBat(const int& choix);

    protected:
    private:
        short _frame;
        short _frameMax;
        short _step;

        std::array< std::array<Batiment *, tailleMatriceY>, tailleMatriceX> _map;
        sf::RenderWindow* _window;
        sf::Sprite _sprFond;
        sf::Texture _textFond;
        sf::Sprite _sprBatiment0;
        sf::RenderTexture _renderBatiment0;
        sf::Sprite _sprBatiment1;
        sf::RenderTexture _renderBatiment1;
        sf::Sprite _sprBatiment2;
        sf::RenderTexture _renderBatiment2;

        sf::Texture _texBoisson0,_texBoisson1,_texBoisson2;
        sf::Texture _texCharbon0,_texCharbon1,_texCharbon2;
        sf::Texture _texChasse0,_texChasse1,_texChasse2;
        sf::Texture _texGlace0,_texGlace1,_texGlace2;
        sf::Texture _texMaison0,_texMaison1,_texMaison2;
        sf::Texture _texPeche0,_texPeche1,_texPeche2;
        sf::Texture _texPierre0,_texPierre1,_texPierre2;
        sf::Texture _texSerre0,_texSerre1,_texSerre2;
        sf::Texture _texStock0,_texStock1,_texStock2;

};

#endif // MAP_H
