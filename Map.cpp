#include "Map.h"
#include "maison.hpp"
#include "peche.hpp"
#include "chasse.hpp"
#include "pierre.hpp"
#include "glace.hpp"
#include "boisson.hpp"
#include "charbon.hpp"
#include "serre.hpp"
#include "stock.hpp"


Map::Map(sf::RenderWindow* window)
{
    //ctor
    _window = window;
    if (!_textFond.loadFromFile("./assets/images/neige0.png")){}

    if (!_texBoisson0.loadFromFile("./assets/images/boisson0.png")){}
    if (!_texBoisson1.loadFromFile("./assets/images/boisson1.png")){}
    if (!_texBoisson2.loadFromFile("./assets/images/boisson2.png")){}

    if (!_texCharbon0.loadFromFile("./assets/images/charbon0.png")){}
    if (!_texCharbon1.loadFromFile("./assets/images/charbon1.png")){}
    if (!_texCharbon2.loadFromFile("./assets/images/charbon2.png")){}

    if (!_texChasse0.loadFromFile("./assets/images/chasse0.png")){}
    if (!_texChasse1.loadFromFile("./assets/images/chasse1.png")){}
    if (!_texChasse2.loadFromFile("./assets/images/chasse2.png")){}

    if (!_texGlace0.loadFromFile("./assets/images/glace0.png")){}
    if (!_texGlace1.loadFromFile("./assets/images/glace1.png")){}
    if (!_texGlace2.loadFromFile("./assets/images/glace2.png")){}

    if (!_texMaison0.loadFromFile("./assets/images/igloo0.png")){}
    if (!_texMaison1.loadFromFile("./assets/images/igloo1.png")){}
    if (!_texMaison2.loadFromFile("./assets/images/igloo2.png")){}

    if (!_texPeche0.loadFromFile("./assets/images/peche0.png")){}
    if (!_texPeche1.loadFromFile("./assets/images/peche1.png")){}
    if (!_texPeche2.loadFromFile("./assets/images/peche2.png")){}

    if (!_texPierre0.loadFromFile("./assets/images/pierre0.png")){}
    if (!_texPierre1.loadFromFile("./assets/images/pierre1.png")){}
    if (!_texPierre2.loadFromFile("./assets/images/pierre2.png")){}

    if (!_texSerre0.loadFromFile("./assets/images/serre0.png")){}
    if (!_texSerre1.loadFromFile("./assets/images/serre1.png")){}
    if (!_texSerre2.loadFromFile("./assets/images/serre2.png")){}

    if (!_texStock0.loadFromFile("./assets/images/stock0.png")){}
    if (!_texStock1.loadFromFile("./assets/images/stock1.png")){}
    if (!_texStock2.loadFromFile("./assets/images/stock2.png")){}

    _frame=0;
    _frameMax=30;
    _step=0;

    _sprFond.setTexture(_textFond,true);
    _sprFond.setTextureRect(sf::IntRect(0,0,64*tailleMatriceX,64*tailleMatriceY));
    _textFond.setRepeated(true);
    _renderBatiment0.create(tailleMatriceX*64, tailleMatriceY*64);
    _sprBatiment0.setTextureRect(sf::IntRect(0,0,64*tailleMatriceX,64*tailleMatriceY));
    _renderBatiment1.create(tailleMatriceX*64, tailleMatriceY*64);
    _sprBatiment1.setTextureRect(sf::IntRect(0,0,64*tailleMatriceX,64*tailleMatriceY));
    _renderBatiment2.create(tailleMatriceX*64, tailleMatriceY*64);
    _sprBatiment2.setTextureRect(sf::IntRect(0,0,64*tailleMatriceX,64*tailleMatriceY));

    for(int i = 0; i < tailleMatriceX; ++i)
    {
      for(int j = 0; j < tailleMatriceY; ++j)
      {
        _map[i][j] = nullptr;
      }
    }

}

void Map::draw()
{
    _window->draw(_sprFond);

    _frame++;

    if (_frame>=_frameMax)
    {
      _frame-=_frameMax;
      _step= (_step+1)%4;
    }

    if (_step==0)
      _window->draw(_sprBatiment0);
    else if (_step==2)
      _window->draw(_sprBatiment2);
    else
      _window->draw(_sprBatiment1);

    /*for(int i = 0; i < tailleMatriceX; ++i)
    {
      for(int j = 0; j < tailleMatriceY; ++j)
      {
        std::cout << (_map[i][j] != nullptr) << " ";
      }

      std::cout  << std::endl;
    }*/

}

bool Map::isEmpty(const std::vector<std::pair<int,int>>& dim, int x, int y)
{
    bool vide=true;

    for (std::pair<int,int> p : dim)
    {
        if(_map[y + p.second][x + p.first])
            vide = false;
    }

    return vide;
}


void Map::addBatimentMap(Batiment * b, const std::vector<std::pair<int,int>>& dim, int x, int y)
{
    //std::cout << dim.size() << std::endl;

    for (std::pair<int,int> p : dim)
    {
        _map[y + p.second][x + p.first] = b;
    }
}

void Map::addBatiment(Batiment * batiment, int x, int y)
{
    sf::Sprite s0, s1, s2;
    s0.setPosition(x*64,y*64);
    s1.setPosition(x*64,y*64);
    s2.setPosition(x*64,y*64);

    //std::cout << batiment->type() << std::endl;

    switch(batiment->type())
    {
    case MAISON:
        s0.setTexture(_texMaison0,true);
        s1.setTexture(_texMaison1,true);
        s2.setTexture(_texMaison2,true);

        //std::cout << Maison::getDimension().size() << std::endl;

        addBatimentMap(batiment, Maison::getDimension(), x, y);
        break;
    case CHASSE:
        s0.setTexture(_texChasse0,true);
        s1.setTexture(_texChasse1,true);
        s2.setTexture(_texChasse2,true);
        addBatimentMap(batiment, Chasse::getDimension(), x, y);
        break;
    case PECHE:
        s0.setTexture(_texPeche0,true);
        s1.setTexture(_texPeche1,true);
        s2.setTexture(_texPeche2,true);
        addBatimentMap(batiment, Peche::getDimension(), x, y);
        break;
    case BOISSON:
        s0.setTexture(_texBoisson0,true);
        s1.setTexture(_texBoisson1,true);
        s2.setTexture(_texBoisson2,true);
        addBatimentMap(batiment, Boisson::getDimension(), x, y);
        break;
    case SERRE:
        s0.setTexture(_texSerre0,true);
        s1.setTexture(_texSerre1,true);
        s2.setTexture(_texSerre2,true);
        addBatimentMap(batiment, Serre::getDimension(), x, y);
        break;
    case MPIERRE:
        s0.setTexture(_texPierre0,true);
        s1.setTexture(_texPierre1,true);
        s2.setTexture(_texPierre2,true);
        addBatimentMap(batiment, Pierre::getDimension(), x, y);
        break;
    case MGLACE:
        s0.setTexture(_texGlace0,true);
        s1.setTexture(_texGlace1,true);
        s2.setTexture(_texGlace2,true);
        addBatimentMap(batiment, Glace::getDimension(), x, y);
        break;
    case MCHARBON:
        s0.setTexture(_texCharbon0,true);
        s1.setTexture(_texCharbon1,true);
        s2.setTexture(_texCharbon2,true);
        addBatimentMap(batiment, Charbon::getDimension(), x, y);
        break;
    case STOCK:
        s0.setTexture(_texStock0,true);
        s1.setTexture(_texStock1,true);
        s2.setTexture(_texStock2,true);
        addBatimentMap(batiment, Stock::getDimension(), x, y);
        break;
    default:
        std::cout << "default" << std::endl;
        break;
    }


    _renderBatiment0.draw(s0);
    _renderBatiment1.draw(s1);
    _renderBatiment2.draw(s2);

    _renderBatiment0.display();
    _renderBatiment1.display();
    _renderBatiment2.display();

    _sprBatiment0.setTexture(_renderBatiment0.getTexture());
    _sprBatiment1.setTexture(_renderBatiment1.getTexture());
    _sprBatiment2.setTexture(_renderBatiment2.getTexture());
}

Map::~Map()
{
    //dtor
}

const sf::Texture& Map::getTextureBat(const int& choix)
{
  switch (choix)
  {
    case MAISON:
      return _texMaison0;

      break;

    case CHASSE:
      return _texChasse0;

      break;

    case PECHE:
      return _texPeche0;

      break;

    case BOISSON:
      return _texBoisson0;

      break;

    case SERRE:
      return _texSerre0;

      break;

    case MPIERRE:
      return _texPierre0;

      break;

    case MGLACE:
      return _texGlace0;

      break;

    case MCHARBON:
      return _texCharbon0;

      break;

    case STOCK:
      return _texStock0;

      break;
  }

}
