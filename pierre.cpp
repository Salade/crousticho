#include "pierre.hpp"
#include "hud.hpp"

std::vector<std::pair<int,int>> Pierre::_dimension;

Pierre::Pierre()
: Batiment("Pierre", 20, 5, 0, 1 * buffFood)
{
  _ressource[GLACE] = 0;
  _ressource[BOIS] = 30;
  _ressource[PIERRE] = 0;
  _ressource[PERSONNE] = 4;
  _ressource[CHARBON] = 0;
  _ressource[BOUFFE] = 0;
}

void Pierre::construction(std::array<int, 6>& stock)
{
  for(int i = 0; i < 6; ++i)
  {
    stock[i] -= _ressource[i];
  }
}

void Pierre::upgrade(std::array<int, 6>& stock)
{
	if (_niveau < _nivMax){

		switch (_niveau)
		{
			case 1:
				_ressource = {{30, 55, 20, 0, 0, 0}};
				_cptFrameMax = 19;
				break;
			case 2:
				_ressource = {{65, 110, 40, 0, 0, 0}};
				_cptFrameMax = 18;
				break;
			case 3:
				_ressource = {{140, 245, 65, 1, 0, 0}};
				_cptFrameMax = 17;
				break;
			case 4:
				_ressource = {{205, 330, 80, 1, 0, 0}};
				_cptFrameMax = 15;
				break;
			default:
				break;
		}
    if(Hud::getHud()->enoughRessources(Hud::getHud()->getRessourceStockees(), _ressource))
    {
      construction(stock);
      _niveau++;
    }
	}
}

void Pierre::majRessource(std::array<int, 6>& stock)
{
  if(_cptFrame == _cptFrameMax)
  {

    stock[PIERRE] += 1 * buffFood;

    _cptFrame = 0;

  }
  else
  {
    ++_cptFrame;
  }
}


const std::vector<std::pair<int,int>>& Pierre::getDimension()
{
	return _dimension;
}

void Pierre::setDimension(const std::vector<std::pair<int,int>>& dimension)
{
	_dimension = dimension;
}

void Pierre::init()
{
  std::vector<std::pair<int, int>> temp;

  temp.push_back(std::make_pair(0, 0));
  temp.push_back(std::make_pair(1, 0));
  temp.push_back(std::make_pair(0, 1));
  temp.push_back(std::make_pair(0, 2));
  temp.push_back(std::make_pair(1, 2));

  setDimension(temp);
}


batiments Pierre::type()
{
  return MPIERRE;
}
