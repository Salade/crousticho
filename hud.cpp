#include "hud.hpp"
#include "maison.hpp"
#include "peche.hpp"
#include "chasse.hpp"
#include "pierre.hpp"
#include "glace.hpp"
#include "boisson.hpp"
#include "charbon.hpp"
#include "serre.hpp"
#include "stock.hpp"

#include <string>
#include <sstream>


Hud * Hud::_hud = nullptr;

Hud::Hud(sf::RenderWindow* window, Map * map)
: _window(window), _camera(sf::FloatRect(0, 0, wWindow, hWindow + hInterface)), _ressourceStockees({{200,200,200,5,50,50}}), _stockMax(6000), _xCamera(tailleMatriceX * 32), _yCamera(tailleMatriceY * 32), _up(0), _down(0), _left(0), _right(0), _choix(-1), _map(map),  _frame(0), _frameMax(0), _stockPlein(false), _habitantMort(false)
{
  _window->setView(_camera);

  if (!_texture.loadFromFile("./assets/images/HUDBar.png")){}

  _sprite.setTexture(_texture,true);
  _sprite.setTextureRect(sf::IntRect(0,0,wInterface,hInterface));

  _sprite.setPosition(sf::Vector2f(_xCamera - wWindow/2 + margeInterface , _yCamera + (hWindow + hInterface)/2 - hInterface));



  if (!_textureSelection.loadFromFile("./assets/images/selection.png")){}

  _spriteSelection.setTexture(_textureSelection,true);
  _spriteSelection.setTextureRect(sf::IntRect(0,0,42,42));


  if(!_font.loadFromFile("./assets/arial.ttf")){};

}

void Hud::start()
{
  _hud = this;

  Batiment * b;

  for(int i = 0; i < 5; ++i)
  {
    b = new Maison();
    addBatiment(b);
    _map->addBatiment(b, tailleMatriceX/2 + i, tailleMatriceY/2);
  }
}

Hud * Hud::getHud()
{
    return _hud;
}

void Hud::tick()
{
  getEvents();
  moveCamera();
  drawButton();

  if(_frame >= _frameMax)
  {
    majStock();
    _frame = 0;
  }
  else
  {
    ++_frame;
  }

  writeRessources();
}

void Hud::keyPressed(const sf::Event& event)
{

  switch (event.key.code)
  {

    case sf::Keyboard::Q:

      _left = 1;
      break;

    case sf::Keyboard::D:

      _right = 1;
      break;

    case sf::Keyboard::S:

      _down = 1;
      break;

    case sf::Keyboard::Z:

      _up = 1;
      break;

    default:
      break;

  }

}

void Hud::keyReleased(const sf::Event& event)
{
  switch (event.key.code)
  {

    case sf::Keyboard::Q:

      _left = 0;
      break;

    case sf::Keyboard::D:

      _right = 0;
      break;

    case sf::Keyboard::S:

      _down = 0;
      break;

    case sf::Keyboard::Z:

      _up = 0;
      break;

    default:
      break;

  }

}

void Hud::clickReleased(const sf::Event& event)
{
  int xClick = event.mouseButton.x;
  int yClick = event.mouseButton.y;
  int i, j, x, y;
  int ecart = 5, ecart_gauche = 20, ecart_haut = 45;
  bool empty;
  Batiment * construction;



  std::array<int, 5> l1 = {MAISON, CHASSE, PECHE, BOISSON, SERRE};
  std::array<int, 5> l2 = {MPIERRE, MGLACE, MCHARBON, STOCK, -1};

  std::array<std::array<int, 5>, 2> boutons = {{l1, l2}};


  if(xClick > 0 && xClick < wWindow && yClick > hWindow && yClick < hWindow + hInterface)
  {

    if(xClick > margeInterface + ecart_gauche && xClick < margeInterface + ecart_gauche + 5 * tailleBouton + 4 * ecart && yClick > hWindow + ecart_haut && yClick < hWindow + ecart_haut + 2 * tailleBouton + ecart)
    {
      i = 0;
      j = 0;
      x = margeInterface + ecart_gauche;
      y = hWindow + ecart_haut;

      while(j < 2 && !(yClick > y + j * (tailleBouton + ecart) && yClick < y + tailleBouton + j * (tailleBouton + ecart)))
      {
        ++j;
      }

      while(i < 5 && !(xClick > x + i * (tailleBouton + ecart) && xClick < x + tailleBouton +  i * (tailleBouton + ecart)))
      {
        ++i;
      }

      //std::cout << j << " " << i << std::endl;

      if(i >= 5 || j >= 2)
      {
        _choix = -1;
      }
      else
      {
        _choix = boutons[j][i];
      }

      //std::cout << _choix << std::endl;
    }


  }


  if(_choix != -1)
  {
    if(!(xClick > margeInterface && xClick < wWindow - margeInterface && yClick > hWindow && yClick < hWindow + hInterface))
    {
      i = (xClick + _xCamera - wWindow/2) / 64;
      j = (yClick + _yCamera - (hWindow+hInterface)/2 ) / 64;


      switch (_choix)
      {
        case MAISON:
          construction = new Maison();

          empty = _map->isEmpty(Maison::getDimension(), i, j);

          break;

        case CHASSE:
          construction = new Chasse();

          empty = _map->isEmpty(Chasse::getDimension(), i, j);

          break;

        case PECHE:
          construction = new Peche();

          empty = _map->isEmpty(Peche::getDimension(), i, j);

          break;

        case BOISSON:
          construction = new Boisson();

          empty = _map->isEmpty(Boisson::getDimension(), i, j);

          break;

        case SERRE:
          construction = new Serre();

          empty = _map->isEmpty(Serre::getDimension(), i, j);

          break;

        case MPIERRE:
          construction = new Pierre();

          empty = _map->isEmpty(Pierre::getDimension(), i, j);

          break;

        case MGLACE:
          construction = new Glace();

          empty = _map->isEmpty(Glace::getDimension(), i, j);

          break;

        case MCHARBON:
          construction = new Charbon();

          empty = _map->isEmpty(Charbon::getDimension(), i, j);

          break;

        case STOCK:
          construction = new Stock();

          empty = _map->isEmpty(Stock::getDimension(), i, j);

          break;
      }


      if(empty && enoughRessources(_ressourceStockees, construction->getRessource()))
      {
        //std::cout << i << " " << j << std::endl;
        _map->addBatiment(construction, i, j);
        addBatiment(construction);
        construction->construction(_ressourceStockees);
        //std::cout << _batiments.size() << std::endl;
      }

    }
  }
}

void Hud::mouseMoved(const sf::Event& event)
{
  float marge = 1.0/20;

  if(event.mouseMove.x < marge * wWindow)
  {
    _left = 1;
  }
  else
  {
    _left = 0;
  }

  if(event.mouseMove.x > wWindow - marge * wWindow)
  {
    _right = 1;
  }
  else
  {
    _right = 0;
  }

  if(event.mouseMove.y < marge * hWindow)
  {
    _up = 1;
  }
  else
  {
    _up = 0;
  }

  if(event.mouseMove.y > hWindow + hInterface - marge * hWindow)
  {
    _down = 1;
  }
  else
  {
    _down = 0;
  }

  if(_choix != -1)
  {


    _spriteVisual.setTexture(_map->getTextureBat(_choix));

    _spriteVisual.setTextureRect(sf::IntRect(0,0,_map->getTextureBat(_choix).getSize().x, _map->getTextureBat(_choix).getSize().y));

    //std::cout << event.mouseMove.x << " " << event.mouseMove.y << std::endl;

    _spriteVisual.setPosition(sf::Vector2f((_xCamera - wWindow/2 + event.mouseMove.x)/64 * 64 , (_yCamera - (hWindow + hInterface)/2 + event.mouseMove.y)/64 * 64));
  }

}

void Hud::getEvents()
{
  sf::Event event;

  while (_window->pollEvent(event))
  {
    // fermeture de la fenêtre lorsque l'utilisateur le souhaite
    switch (event.type)
    {
        // fenêtre fermée
        case sf::Event::Closed:
            _window->close();
            break;

        // touche pressée
        case sf::Event::KeyPressed:

          keyPressed(event);

          break;

        //touche relachée
        case sf::Event::KeyReleased:

          keyReleased(event);

          break;

        case sf::Event::MouseMoved:

          mouseMoved(event);

          break;

        case sf::Event::MouseButtonReleased:

          clickReleased(event);

          break;

        // on ne traite pas les autres types d'évènements
        default:
            break;
    }
  }

}

void Hud::moveCamera()
{
  int speed = 2;

  _xCamera += (_right - _left) * speed;
  _yCamera += (_down - _up) * speed;

  _camera.setCenter(sf::Vector2f(_xCamera, _yCamera));
  _window->setView(_camera);

  _sprite.setPosition(sf::Vector2f(_xCamera - wWindow/2 + margeInterface , _yCamera + (hWindow + hInterface)/2 - hInterface));
}

void Hud::drawButton()
{
  if(_choix != -1)
  {
    _window->draw(_spriteVisual);
  }
  _window->draw(_sprite);
  if (_choix!=-1)
  {
      int c= _choix;
     _spriteSelection.setPosition(sf::Vector2f((_xCamera - wWindow/2 + margeInterface +15 +(c%5)*37 ),(_yCamera + (hWindow + hInterface)/2 - hInterface+40 + ((int)(c/5))*37)));
     _window->draw(_spriteSelection);
  }
}

void Hud::addBatiment(Batiment * bat)
{
  _batiments.push_back(bat);
}

void Hud::majStock()
{

	int stock = 0;
	int espaceDispo;
  std::array<int, 6> save;

  for(int i = 0; i < 6; ++i)
  {
    save[i] = _ressourceStockees[i];
  }

	for (int i=0;i<6;i++)
    if(i != PERSONNE)
	   stock += _ressourceStockees[i];

	_stockMax = Stock::getNbStocks()*6000 + 1000;

	espaceDispo = _stockMax - stock;

  //std::cout << _stockMax << " " << stock << std::endl;

	if (espaceDispo < 0)
  {
		espaceDispo = 0;
	}

	for (Batiment * b : _batiments)
  {
    if(b->type() != MAISON)
    {
      if (espaceDispo + b->getConso() - b->getProd() > 0)
      {
        _stockPlein = false;
        //std::cout << b->getConso() << " " << b->getProd() << std::endl;
        b->majRessource(_ressourceStockees);
      }
      else
      {
        _stockPlein = true;
        //std::cout << b->type() << " peut pas produire !" << std::endl;
      }
    }
    else
    {
      //std::cout << b->type() << " est une maison !" << std::endl;
      b->majRessource(_ressourceStockees);
    }
	}

  for(int i = 0; i < 6; ++i)
  {
    _changements[i] += _ressourceStockees[i] - save[i];
  }
}

bool Hud::enoughRessources(const std::array<int, 6>& stock, const std::array<int, 6>& demande)
{
  bool result = true;

  for(int i = 0; i < 6; ++i)
  {
    result = result && (demande[i] <= stock[i]);
  }

  return result;
}

void Hud::writeRessources() const
{
  std::array<int , 6> need;
  Batiment * construction;

  int x = margeInterface + 20 + 5 * 32 + 4 * 5 + 115;
  int y = 10 + 2 * 32 + 5;

  sf::Text text;
  text.setFont(_font);
  text.setCharacterSize(12);


  for(int i = 0; i < 6; ++i)
  {
    std::ostringstream ss;
    ss << _ressourceStockees[i];
    text.setString(ss.str());
    text.setPosition(sf::Vector2f(_xCamera - wWindow/2 + x + i * 37 , _yCamera + (hWindow + hInterface)/2 - y));
    _window->draw(text);
  }

  if(_choix != -1)
  {
    switch (_choix)
    {
      case MAISON:
        construction = new Maison();

        break;

      case CHASSE:
        construction = new Chasse();

        break;

      case PECHE:
        construction = new Peche();

        break;

      case BOISSON:
        construction = new Boisson();

        break;

      case SERRE:
        construction = new Serre();

        break;

      case MPIERRE:
        construction = new Pierre();

        break;

      case MGLACE:
        construction = new Glace();

        break;

      case MCHARBON:
        construction = new Charbon();

        break;

      case STOCK:
        construction = new Stock();

        break;
    }

    need = construction->getRessource();

    for(int i = 0; i < 6; ++i)
    {
      std::ostringstream ss;
      ss << need[i];
      text.setString(ss.str());
      text.setPosition(sf::Vector2f(_xCamera - wWindow/2 + x + i * 37 , _yCamera + (hWindow + hInterface)/2 - y + 30));
      _window->draw(text);
    }

    delete construction;
  }
  else
  {
    for(int i = 0; i < 6; ++i)
    {
      std::ostringstream ss;
      ss << _changements[i];
      text.setString(ss.str());
      text.setPosition(sf::Vector2f(_xCamera - wWindow/2 + x + i * 37 , _yCamera + (hWindow + hInterface)/2 - y + 30));
      _window->draw(text);
    }
  }

  //messages d'avertissements

  text.setCharacterSize(30);

  if(_habitantMort)
  {
    text.setString("Un habitant est mort !");
    text.setPosition(sf::Vector2f(_xCamera - 11 * 15, _yCamera - (hWindow + hInterface)/2 + 5));
    _window->draw(text);
  }

  if(_stockPlein)
  {
    text.setString("Le stock est plein !");
    text.setPosition(sf::Vector2f(_xCamera - 10 * 15, _yCamera - (hWindow + hInterface)/2 + 40));
    _window->draw(text);
  }

}

const std::array<int, 6>& Hud::getRessourceStockees() const
{
  return _ressourceStockees;
}


void Hud::setHabitantMort(const bool& b)
{
  _habitantMort = b;
}


Hud::~Hud()
{
    for(unsigned int i=0;i<_batiments.size();i++ )
        delete _batiments[i];

}
