#ifndef __CHASSE__
#define __CHASSE__

#include "batiment.hpp"

class Chasse : public Batiment{

	private:

		static std::vector<std::pair<int,int>> _dimension;

	public :

		Chasse();

		void construction(std::array<int, 6>&);
		void upgrade(std::array<int, 6>&);
		void majRessource(std::array<int, 6>&);

		static const std::vector<std::pair<int,int>>& getDimension();

		static void setDimension(const std::vector<std::pair<int,int>>&);

		static void init();

		batiments type();

};




#endif
