#ifndef BOISSON_CPP
#define BOISSON_CPP

#include "data.hpp"
#include "batiment.hpp"

class Boisson : public Batiment
{
  private:

    static std::vector<std::pair<int,int>> _dimension;

  public:

    Boisson();

    static const std::vector<std::pair<int,int>>& getDimension();

    static void setDimension(const std::vector<std::pair<int,int>>&);

    void construction(std::array<int, 6>&);

    void upgrade(std::array<int, 6>&);

    void majRessource(std::array<int, 6>&);

    static void init();

    batiments type();
};
#endif
