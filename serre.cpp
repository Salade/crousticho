#include "serre.hpp"
#include "hud.hpp"

std::vector<std::pair<int,int>> Serre::_dimension;

Serre::Serre()
: Batiment("Serre", 20, 5, 0, 1 * buffFood)
{
  _ressource[GLACE] = 20;
  _ressource[BOIS] = 0;
  _ressource[PIERRE] = 20;
  _ressource[PERSONNE] = 3;
  _ressource[CHARBON] = 0;
  _ressource[BOUFFE] = 0;
}

void Serre::construction(std::array<int, 6>& stock)
{
  for(int i = 0; i < 6; ++i)
  {
    stock[i] -= _ressource[i];
  }
}

void Serre::upgrade(std::array<int, 6>& stock)
{
	if (_niveau < _nivMax){

		switch (_niveau)
		{
			case 1:
				_ressource = {{45, 20, 55, 0, 0, 0}};
				_cptFrameMax = 19;
				break;
			case 2:
				_ressource = {{100, 40, 120, 0, 0, 0}};
				_cptFrameMax = 18;
				break;
			case 3:
				_ressource = {{160, 65, 190, 1, 0, 0}};
				_cptFrameMax = 17;
				break;
			case 4:
				_ressource = {{215, 80, 280, 1, 0, 0}};
				_cptFrameMax = 15;
				break;
			default:
				break;
		}
    if(Hud::getHud()->enoughRessources(Hud::getHud()->getRessourceStockees(), _ressource))
    {
      construction(stock);
      _niveau++;
    }
	}
}

void Serre::majRessource(std::array<int, 6>& stock)
{
  if(_cptFrame == _cptFrameMax)
  {
    stock[BOIS] += 1 * buffFood;

    _cptFrame = 0;

  }
  else
  {
    ++_cptFrame;
  }
}


const std::vector<std::pair<int,int>>& Serre::getDimension()
{
	return _dimension;
}

void Serre::setDimension(const std::vector<std::pair<int,int>>& dimension)
{
	_dimension = dimension;
}

void Serre::init()
{
  std::vector<std::pair<int, int>> temp;

  temp.push_back(std::make_pair(0, 0));
  temp.push_back(std::make_pair(1, 0));
  temp.push_back(std::make_pair(0, 1));
  temp.push_back(std::make_pair(1, 1));
  temp.push_back(std::make_pair(2, 0));
  temp.push_back(std::make_pair(2, 1));

  setDimension(temp);
}

batiments Serre::type()
{
  return SERRE;
}
