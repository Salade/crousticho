#include "peche.hpp"
#include "hud.hpp"

std::vector<std::pair<int,int>> Peche::_dimension;

Peche::Peche()
: Batiment("Peche", 75, 5, 5, 3 * buffFood)
{
  _ressource[GLACE] = 15;
  _ressource[BOIS] = 5;
  _ressource[PIERRE] = 30;
  _ressource[PERSONNE] = 2;
  _ressource[CHARBON] = 0;
  _ressource[BOUFFE] = 0;
}

void Peche::construction(std::array<int, 6>& stock)
{
  for(int i = 0; i < 6; ++i)
  {
    stock[i] -= _ressource[i];
  }
}

void Peche::upgrade(std::array<int, 6>& stock)
{
	if (_niveau < _nivMax){

		switch (_niveau)
		{
			case 1:
				_ressource = {{45, 20, 60, 0, 0, 0}};
				_cptFrameMax = 72;
				break;
			case 2:
				_ressource = {{100, 75, 160, 1, 0, 0}};
				_cptFrameMax = 69;
				break;
			case 3:
				_ressource = {{210, 180, 340, 1, 0, 0}};
				_cptFrameMax = 63;
				break;
			case 4:
				_ressource = {{495, 345, 600, 2, 0, 0}};
				_cptFrameMax = 55;
				break;
			default:
				break;
		}
    if(Hud::getHud()->enoughRessources(Hud::getHud()->getRessourceStockees(), _ressource))
    {
      construction(stock);
      _niveau++;
    }
	}
}

void Peche::majRessource(std::array<int, 6>& stock)
{
  if(_cptFrame == _cptFrameMax)
  {
    if(stock[BOIS] >= 4 && stock[GLACE] >= 1)
    {
      stock[BOIS] -= 4;
      stock[GLACE] -= 1;
      stock[BOUFFE] += 3 * buffFood;
    }

    _cptFrame = 0;

  }
  else
  {
    ++_cptFrame;
  }
}


const std::vector<std::pair<int,int>>& Peche::getDimension()
{
	return _dimension;
}

void Peche::setDimension(const std::vector<std::pair<int,int>>& dimension)
{
	_dimension = dimension;
}

void Peche::init()
{
  std::vector<std::pair<int, int>> temp;

  temp.push_back(std::make_pair(0, 0));
  temp.push_back(std::make_pair(1, 0));
  temp.push_back(std::make_pair(0, 1));

  setDimension(temp);
}

batiments Peche::type()
{
  return PECHE;
}
