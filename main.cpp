#include "data.hpp"
#include "Map.h"

#include "maison.hpp"
#include "chasse.hpp"
#include "peche.hpp"
#include "boisson.hpp"
#include "serre.hpp"
#include "pierre.hpp"
#include "glace.hpp"
#include "hud.hpp"
#include "charbon.hpp"
#include "stock.hpp"

int main(int, char**)
{

  //création de la fenêtre
  sf::RenderWindow window(sf::VideoMode(wWindow, hWindow + hInterface), "CroustiChO");

  //60 fps
  window.setFramerateLimit(60);

  Maison::init();
  Chasse::init();
  Peche::init();
  Boisson::init();
  Serre::init();
  Pierre::init();
  Glace::init();
  Charbon::init();
  Stock::init();

  Map m(&window);
  Hud hud(&window, &m);

  hud.start();


  // on fait tourner le programme tant que la fenêtre n'a pas été fermée
  while (window.isOpen())
  {
    // effacement de la fenêtre en noir
    window.clear(sf::Color::Black);
    m.draw();

    hud.tick();

    // fin de la frame courante, affichage de tout ce qu'on a dessiné
    window.display();
  }

  return 0;
}
