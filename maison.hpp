#ifndef MAISON_HPP
#define MAISON_HPP

#include "data.hpp"
#include "batiment.hpp"

class Maison : public Batiment
{
  private:

    static std::vector<std::pair<int,int>> _dimension;

    int _capaPopulation;
    int _habitants;
    int _famine;

  public:
    Maison();

    static const std::vector<std::pair<int,int>>& getDimension();

    static void setDimension(const std::vector<std::pair<int,int>>&);

    void construction(std::array<int, 6>&);

    void upgrade(std::array<int, 6>&);

    void majRessource(std::array<int, 6>&);

    static void init();

    batiments type();
};
#endif
