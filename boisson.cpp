#include "boisson.hpp"
#include "hud.hpp"

std::vector<std::pair<int,int>> Boisson::_dimension;

Boisson::Boisson()
: Batiment("Boisson", 40, 5, 2, 1 * buffFood)
{
  _ressource[GLACE] = 20;
  _ressource[BOIS] = 0;
  _ressource[PIERRE] = 20;
  _ressource[PERSONNE] = 3;
  _ressource[CHARBON] = 0;
  _ressource[BOUFFE] = 0;
}

void Boisson::construction(std::array<int, 6>& stock)
{
  for(int i = 0; i < 6; ++i)
  {
    stock[i] -= _ressource[i];
  }
}

void Boisson::upgrade(std::array<int, 6>& stock)
{
	if (_niveau < _nivMax){
		switch (_niveau)
		{
			case 1:
        _ressource = {{15, 45, 60, 0, 0, 0}};
				_cptFrameMax = 38;
				break;
			case 2:
				_ressource = {{25, 70, 95, 0, 0, 0}};
				_cptFrameMax = 36;
				break;
			case 3:
				_ressource = {{50, 140, 160, 1, 0, 0}};
				_cptFrameMax = 32;
				break;
			case 4:
				_ressource = {{90, 225, 300, 1, 0, 0}};
				_cptFrameMax = 27;
				break;
			default:
				break;
		}

    if(Hud::getHud()->enoughRessources(Hud::getHud()->getRessourceStockees(), _ressource))
    {
      construction(stock);
      _niveau++;
    }
	}
}

void Boisson::majRessource(std::array<int, 6>& stock)
{
  if(_cptFrame == _cptFrameMax)
  {
    if(stock[GLACE] >= 1 && stock[CHARBON] >= 1)
    {
      stock[GLACE] -= 1;
      stock[CHARBON] -= 1;
      stock[BOUFFE] += 1 * buffFood;
    }

    _cptFrame = 0;

  }
  else
  {
    ++_cptFrame;
  }
}


const std::vector<std::pair<int,int>>& Boisson::getDimension()
{
	return _dimension;
}

void Boisson::setDimension(const std::vector<std::pair<int,int>>& dimension)
{
	_dimension = dimension;
}

void Boisson::init()
{
  std::vector<std::pair<int, int>> temp;

  temp.push_back(std::make_pair(0, 0));
  temp.push_back(std::make_pair(1, 0));
  temp.push_back(std::make_pair(1, 1));

  setDimension(temp);
}

batiments Boisson::type()
{
  return BOISSON;
}
