#include "batiment.hpp"

Batiment::Batiment(const std::string nom, const int& cptFrameMax, const int& nivMax, const int& conso ,const int& prod)
:_nom(nom), _cptFrame(0), _cptFrameMax(cptFrameMax / divDelta),_niveau(1), _nivMax(nivMax), _conso(conso), _prod(prod)
{

}

std::string Batiment::getNom() const{

	return _nom;
}

int Batiment::getNiveau() const{

	return _niveau;
}

const std::array<int, 6>& Batiment::getRessource(){

	return _ressource;
}

const int& Batiment::getConso() const
{
	return _conso;
}

const int& Batiment::getProd() const
{
	return _prod;
}
